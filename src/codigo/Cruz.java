/*
 * mi primera clase java

La clase circulo permite almacenar un circulo ademas de su posicion en la pantalla, si esta relleno
o no , su color etc
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;


/**
 *
 * @author Toñin
 */
public class Cruz extends Polygon{
    //esto es una propiedad del circulo, le estamos asignando un color
    //y se nombra con un "public"
    public Color color = null; 
    
    // para asignar relleno utilizamos un public boolean, para poder usarlo
    // en la VentanaDibujo.
    public boolean relleno = false;
    
  
    // le ponemos el "_" para difeenciarlos uno de la variable. es recomendable ponerle algo delante
    //en este caso "_"
    public Cruz (int _x, int _y, int _width, Color _color, boolean _relleno){
        
        
        
        addPoint (_x-_width, _y+3*_width);
        addPoint (_x + _width, _y+3*_width);
        addPoint (_x+ _width, _y+_width);
        addPoint (_x+3*_width, _y+_width);
        addPoint (_x+3*_width, _y-_width);
        addPoint (_x+ _width, _y-_width);
        addPoint (_x+ _width, _y-3*_width);
        addPoint (_x- _width, _y-3*_width);
        addPoint (_x- _width, _y-_width);
        addPoint (_x-3*_width, _y-_width);
        addPoint (_x-3*_width, _y+_width);
        addPoint (_x-_width, _y+_width);

        
        
        this.relleno = _relleno;
       
        this.color = _color; 
        
    }
        public void pintaYColorea (Graphics2D g2){
            g2.setColor(this.color);
            
            if (this.relleno){
                g2.fill(this);
            }
            else{
                g2.draw (this);
    }
        }
}
