/*
 * mi primera clase java

La clase circulo permite almacenar un circulo ademas de su posicion en la pantalla, si esta relleno
o no , su color etc
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.util.Random;

/**
 *
 * @author Toñin
 */
public class Circulo extends Ellipse2D.Double{
    //esto es una propiedad del circulo, le estamos asignando un color
    //y se nombra con un "public"
    public Color color = null; 
    
    // para asignar relleno utilizamos un public boolean, para poder usarlo
    // en la VentanaDibujo.
    public boolean relleno = false;
    
  
    // le ponemos el "_" para difeenciarlos uno de la variable. es recomendable ponerle algo delante
    //en este caso "_"
    public Circulo (int _x, int _y, int _width, Color _color, boolean _relleno){
        this.relleno = _relleno;
        this.x = _x - _width/2;
        this.y = _y- _width/2;
        this.width = _width;
        this.height = _width;
        this.color = _color; 
        
    }
        public void pintaYColorea (Graphics2D g2){
            g2.setColor(this.color);
            
            if (this.relleno){
                g2.fill(this);
            }
            else{
                g2.draw (this);
    }
        }
}
