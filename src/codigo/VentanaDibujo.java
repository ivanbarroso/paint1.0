/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import static jdk.nashorn.internal.objects.ArrayBufferView.buffer;
import static jdk.nashorn.internal.objects.ArrayBufferView.buffer;

/**
 *
 * @author Toñin
 */
public class VentanaDibujo extends javax.swing.JFrame {
    
    
    private static int DIMENSION_ARRAY = 100;
    //imagen en lña que pintare los circulos 
    //es una variable parecida a un image pero acelerado
    BufferedImage buffer = null;
    
    //indica el buffer del array que estoy utilizando
    
    
    //indica el numero de circulos que hay en el array
    int indice = 0;
    
    //declaro el array en el que voy a guardar los circulos
    //Circulo []listaCirculos = new Circulo [DIMENSION_ARRAY];
    
    //ahora en esta version utilizare un ArrayList
    ArrayList listaForma = new ArrayList();
    
    //Variable que almacena el tipo de forma que estoy dibujando
    //SI vale 0==> dibujo circulos
    //si vale 1 ==> dibujo triangulos
    
    int forma = 0;
    
    //Variable para almacenar el color elegido
    Color coloElegido = Color.BLACK;
    
    //Variables para almacenar la posicion en la que se empieza a dibujar la forma 
    int posX = 0;
    int posY = 0;
    
    
    /**
     * Creates new form VentanaDibujo
     */
    public VentanaDibujo() {
        initComponents();
        
        //creo un buffer del tamaño jpanel1
        buffer = (BufferedImage)jPanel1.createImage(jPanel1.getWidth(), jPanel1.getHeight());
        //creo la parte modificable de la imagen osea donde vamos a dibujar
        buffer.createGraphics();
        Graphics2D g2 = (Graphics2D) buffer.getGraphics();
        g2.setColor(Color.BLUE);
        g2.fillRect(0, 0, jPanel1.getWidth(), jPanel1.getHeight());
        //inicializo el array de buffers
//        for (int i=0; i<listaCirculos.length; i++){
//            listaCirculos [i] = new Circulo ();   
//        }
        
        
        
       // for (int i=0; i<listaCirculos.length; i++){
           // listaDeshacer [i] = (BufferedImage)jPanel1.createImage(jPanel1.getWidth(), jPanel1.getHeight());
           // listaDeshacer [i]. createGraphics();
           // g2 = (Graphics2D) listaDeshacer[i].getGraphics();
            //g2.setColor(Color.BLUE);
           // g2.fillRect(0,0, jPanel1.getWidth(),jPanel1.getHeight());
            
       // }
       

    }
    
    //private int calculaBuffer(int miIndice){
       // if (miIndice % DIMENSION_ARRAY <= DIMENSION_ARRAY){
        // return miIndice % DIMENSION_ARRAY;
    //}
        //else {
           // return 0;
        //}
       
        
   // }
    
private boolean chequeaPunto (int x, int y){
    boolean contiene = false;
    for (int i=0; i<listaForma.size(); i++){
        if(((Shape)listaForma.get(i)).contains(x,y));
        //si en algun momento en contains devuelve true
        //es por que el punto que he pasado esta en una forma
        //de las que tengo guardadas en el arraylist
        contiene=true;
    }
    return contiene;
}

    @Override
    public void paint (Graphics g){
        super.paint(g);
        //Graphics2D es una gran lñibreria que te perimite varias posibilidades de dibujo
        Graphics2D g2 = (Graphics2D) buffer.getGraphics();
        //dibujo un cuadrado en blanco del tamaño del buffer
        
        g2.setColor(Color.BLUE);
        g2.fillRect(0, 0, jPanel1.getWidth(), jPanel1.getHeight());
        
        //dinujo todos los elementos del array menores que el indice
        for (int i=0; i<listaForma.size(); i++){
            Shape aux;
            if (listaForma.get (i) instanceof Circulo ){
                ((Circulo) listaForma.get(i)).pintaYColorea(g2);
            }
            if (listaForma.get (i) instanceof Triangulo ){
                ((Triangulo) listaForma.get(i)).pintaYColorea(g2);
            }
            if (listaForma.get (i) instanceof Cuadrado ){
                ((Cuadrado) listaForma.get(i)).pintaYColorea(g2);
            }
            if (listaForma.get (i) instanceof rombo ){
                ((rombo) listaForma.get(i)).pintaYColorea(g2);
            }
            if (listaForma.get (i) instanceof Cruz ){
                ((Cruz) listaForma.get(i)).pintaYColorea(g2);
            }
            
            //Circulo aux = (Circulo) listaNuevaCirculos.get(i);
           
            //leo el color del crculo 0

            }
        
        
        //apunto al jPanel1
        g2 = (Graphics2D) jPanel1.getGraphics();
        g2.drawImage(buffer, 0, 0, jPanel1.getWidth(), jPanel1.getHeight(),null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jColorChooser1 = new javax.swing.JColorChooser();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jSlider1 = new javax.swing.JSlider();
        jLabel1 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();

        jDialog1.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jDialog1.getContentPane().add(jColorChooser1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, 500, 220));

        jButton6.setText("Aceptar");
        jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton6MousePressed(evt);
            }
        });
        jDialog1.getContentPane().add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 270, -1, -1));

        jButton7.setText("Cancelar");
        jDialog1.getContentPane().add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 270, -1, -1));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel1MouseDragged(evt);
            }
        });
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel1MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 820, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 380, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 820, 380));

        jButton1.setIcon(new javax.swing.ImageIcon("C:\\Users\\Toñin\\Desktop\\ivan barroso clemente\\Entorno de desarrollo\\Programacion\\2ºEVALUACION\\botones de paint\\deshacerdefinitivo.png")); // NOI18N
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton1MousePressed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 40, 40));

        jButton2.setIcon(new javax.swing.ImageIcon("C:\\Users\\Toñin\\Desktop\\ivan barroso clemente\\Entorno de desarrollo\\Programacion\\2ºEVALUACION\\botones de paint\\borrartodo.png")); // NOI18N
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton2MousePressed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 10, 30, 30));

        jSlider1.setToolTipText("");
        jSlider1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jSlider1MouseDragged(evt);
            }
        });
        getContentPane().add(jSlider1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, -1, -1));

        jLabel1.setText("50");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 10, -1, 20));

        jButton3.setIcon(new javax.swing.ImageIcon("C:\\Users\\Toñin\\Desktop\\ivan barroso clemente\\Entorno de desarrollo\\Programacion\\2ºEVALUACION\\botones de paint\\circulodefinitivo.png")); // NOI18N
        jButton3.setPreferredSize(new java.awt.Dimension(30, 23));
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton3MousePressed(evt);
            }
        });
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 10, 40, 20));

        jButton4.setIcon(new javax.swing.ImageIcon("C:\\Users\\Toñin\\Desktop\\ivan barroso clemente\\Entorno de desarrollo\\Programacion\\2ºEVALUACION\\botones de paint\\triangulodefinitivo.png")); // NOI18N
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton4MousePressed(evt);
            }
        });
        getContentPane().add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 10, 40, 20));

        jButton5.setIcon(new javax.swing.ImageIcon("C:\\Users\\Toñin\\Desktop\\ivan barroso clemente\\Entorno de desarrollo\\Programacion\\2ºEVALUACION\\botones de paint\\gamacolores.png")); // NOI18N
        jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton5MousePressed(evt);
            }
        });
        getContentPane().add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 10, 30, 30));

        jButton8.setIcon(new javax.swing.ImageIcon("C:\\Users\\Toñin\\Desktop\\ivan barroso clemente\\Entorno de desarrollo\\Programacion\\2ºEVALUACION\\botones de paint\\cuadradodefinitivo1.png")); // NOI18N
        jButton8.setPreferredSize(new java.awt.Dimension(20, 29));
        jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton8MousePressed(evt);
            }
        });
        getContentPane().add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 10, 40, 20));

        jButton9.setIcon(new javax.swing.ImageIcon("C:\\Users\\Toñin\\Desktop\\ivan barroso clemente\\Entorno de desarrollo\\Programacion\\2ºEVALUACION\\botones de paint\\rombodefinitivo.png")); // NOI18N
        jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton9MousePressed(evt);
            }
        });
        getContentPane().add(jButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 10, 40, 20));

        jButton10.setIcon(new javax.swing.ImageIcon("C:\\Users\\Toñin\\Desktop\\ivan barroso clemente\\Entorno de desarrollo\\Programacion\\2ºEVALUACION\\botones de paint\\cruz.png")); // NOI18N
        jButton10.setMaximumSize(new java.awt.Dimension(30, 23));
        jButton10.setMinimumSize(new java.awt.Dimension(30, 23));
        jButton10.setPreferredSize(new java.awt.Dimension(30, 23));
        jButton10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton10MousePressed(evt);
            }
        });
        getContentPane().add(jButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 10, 40, 20));

        jButton11.setIcon(new javax.swing.ImageIcon("C:\\Users\\Toñin\\Desktop\\ivan barroso clemente\\Entorno de desarrollo\\Programacion\\2ºEVALUACION\\botones de paint\\estrelladefinitiva.png")); // NOI18N
        getContentPane().add(jButton11, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 10, 40, 20));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MousePressed
       if (listaForma.size()>0){
           listaForma.remove(listaForma.size()-1);
           
            repaint();
       }
        
    }//GEN-LAST:event_jButton1MousePressed

    private void jButton2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MousePressed
        listaForma.clear();
        repaint();
    }//GEN-LAST:event_jButton2MousePressed

    private void jSlider1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jSlider1MouseDragged
        jLabel1.setText(String.valueOf(jSlider1.getValue()));
    }//GEN-LAST:event_jSlider1MouseDragged

    private void jButton3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MousePressed
        forma = 0;
    }//GEN-LAST:event_jButton3MousePressed

    private void jButton4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MousePressed
       forma = 1;
    }//GEN-LAST:event_jButton4MousePressed

    private void jButton5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton5MousePressed
       jDialog1.setSize (600, 400);
        jDialog1.setVisible(true);
    }//GEN-LAST:event_jButton5MousePressed

    private void jButton6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton6MousePressed
        coloElegido = jColorChooser1.getColor();
         jDialog1.setVisible(false);
        
    }//GEN-LAST:event_jButton6MousePressed

    private void jButton8MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton8MousePressed
        forma = 2;
    }//GEN-LAST:event_jButton8MousePressed

    private void jButton9MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MousePressed
        forma = 3;
    }//GEN-LAST:event_jButton9MousePressed

    private void jButton10MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton10MousePressed
        forma = 4;
    }//GEN-LAST:event_jButton10MousePressed

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
               
        int radio = jSlider1.getValue();
        int ancho = jSlider1.getValue()/2;
        int alto = jSlider1.getValue();
        if ((chequeaPunto(evt.getX(),evt.getY()))){
            //hay un elemento en esas coordenadas en las que he hecho clic
            System.out.println("HAY UN OBJETO!!");
            
        }
        switch (forma){
            case 0: listaForma.add ( new Circulo (evt.getX(), evt.getY(), radio, coloElegido, true)); break;
            case 1: listaForma.add ( new Triangulo (evt.getX(), evt.getY(), radio, coloElegido, true)); break;
            case 2: listaForma.add ( new Cuadrado (evt.getX(), evt.getY(), radio, coloElegido, true)); break;
            case 3: listaForma.add ( new rombo (evt.getX(), evt.getY(),alto,ancho,  coloElegido, true)); break;
            case 4: listaForma.add ( new Cruz (evt.getX(), evt.getY(),radio,  coloElegido, true)); break;
        }
       
        repaint();
        
        //creo el circulo en la posicion en la que se ha producido el click
        //Ellipse2D.Double circulo = new Ellipse2D.Double(evt.getX()-20,evt.getY()-20,20,20);
        //apunto al buffer
       // Graphics2D g2 =(Graphics2D) listaDeshacer[calculaBuffer (indice+1)].getGraphics();
       // g2.drawImage(listaDeshacer[calculaBuffer(indice)], 0, 0, jPanel1.getWidth(), jPanel1.getHeight(),null);
        //dibujo el circulo en el buffer
        //g2.fill(circulo);
        
        
        
        //ñapa para que el programa no salga del array

        
        
        System.out.println(indice);
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jPanel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MousePressed
       //int ancho = jSlider1.getValue()/2;
        //int alto = jSlider1.getValue();
        int radio = jSlider1.getValue();
        
       // posX = evt.getX();
        //posY = evt.getY();
        
        switch (forma){
            case 0: listaForma.add ( new Circulo (evt.getX(), evt.getY(), 1, coloElegido, true)); break;
            case 1: listaForma.add ( new Triangulo (evt.getX(), evt.getY(), 1, coloElegido, true)); break;
            case 2: listaForma.add ( new Cuadrado (evt.getX(), evt.getY(), radio, coloElegido, true)); break;
            case 3: listaForma.add ( new rombo (evt.getX(), evt.getY(),1,1,  coloElegido, true)); break;
            case 4: listaForma.add ( new Cruz (evt.getX(), evt.getY(),1,  coloElegido, true)); break;
        }
        
    }//GEN-LAST:event_jPanel1MousePressed

    private void jPanel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseDragged
//        int ancho = jSlider1.getValue()/2;
//        int alto = jSlider1.getValue();
        
        switch (forma){
        case 0: {
            //Leo el ultimo elemento de la lista. se que añadio en el MousePressed
        Circulo aux = (Circulo) listaForma.get(listaForma.size()-1);
        int radio = (int)(evt.getX() - aux.x);
        aux.width = radio;
        aux.height = radio;
    } 
        
        break;
    
    }
       repaint();
    
       
    }//GEN-LAST:event_jPanel1MouseDragged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaDibujo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaDibujo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaDibujo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaDibujo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaDibujo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JColorChooser jColorChooser1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSlider jSlider1;
    // End of variables declaration//GEN-END:variables
}
