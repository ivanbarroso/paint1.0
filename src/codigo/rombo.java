/*
 * mi primera clase java

La clase circulo permite almacenar un circulo ademas de su posicion en la pantalla, si esta relleno
o no , su color etc
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;


/**
 *
 * @author Toñin
 */
public class rombo extends Polygon{
    //esto es una propiedad del circulo, le estamos asignando un color
    //y se nombra con un "public"
    public Color color = null; 
    
    // para asignar relleno utilizamos un public boolean, para poder usarlo
    // en la VentanaDibujo.
    public boolean relleno = false;
    
  
    // le ponemos el "_" para difeenciarlos uno de la variable. es recomendable ponerle algo delante
    //en este caso "_"
    public rombo (int _x, int _y, int _width,int _height, Color _color, boolean _relleno){
        
        this. npoints = 4;
        
        this.xpoints[0] = _x - _width/2;
        this.ypoints[0] = _y;
        
        this.xpoints[1] = _x;
        this.ypoints[1] = _y + _width;
        
        this.xpoints[2] = _x +_width/2;
        this.ypoints[2] = _y;
        
        this.xpoints[3] = _x ;
        this.ypoints[3] = _y -_width;
        
        
        this.relleno = _relleno;
       
        this.color = _color; 
        
    }
        public void pintaYColorea (Graphics2D g2){
            g2.setColor(this.color);
            
            if (this.relleno){
                g2.fill(this);
            }
            else{
                g2.draw (this);
    }
        }
}
