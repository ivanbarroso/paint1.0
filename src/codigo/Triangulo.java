/*
 * mi primera clase java

La clase triangulo permite almacenar un circulo ademas de su posicion en la pantalla, si esta relleno
o no , su color etc
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.Random;


/**
 *
 * @author Toñin
 */
public class Triangulo extends Polygon{
    //esto es una propiedad del circulo, le estamos asignando un color
    //y se nombra con un "public"
    public Color color = null; 
    
    // para asignar relleno utilizamos un public boolean, para poder usarlo
    // en la VentanaDibujo.
    public boolean relleno = false;
    
  
    // le ponemos el "_" para difeenciarlos uno de la variable. es recomendable ponerle algo delante
    //en este caso "_"
    public Triangulo (int _x, int _y, int _width, Color _color, boolean _relleno){
       
        this.npoints = 3; 
       
        this.xpoints[0] = _x ;
        this.ypoints[0] = _y - _width/2;
        
        this.xpoints[1] = _x + _width/2;
        this.ypoints[1] = _y + _width/2;
        
        this.xpoints[2] = _x - _width/2;
        this.ypoints[2] = _y + _width/2;
        
         
        this.color =  _color;
        
        this.relleno = _relleno;
        
    }
            public void pintaYColorea (Graphics2D g2){
            g2.setColor(this.color);
            
            if (this.relleno){
                g2.fill(this);
            }
            else{
                g2.draw (this);
    }
        }
            //JavaShape.com
            
            //dibujar un cuadrado en otra clase privada llamada "cuadrado"
                //con la dimension rectangle2D.Double
            //dibujar un rombo en otra clase privada llamada rombo
                //con la dimension Polygon y 4 npoints=4
            //dibujar una cruz enotra clase privada llamada cruz
                //con un constructor/dimension polygon y npoints=12
            
}

    

